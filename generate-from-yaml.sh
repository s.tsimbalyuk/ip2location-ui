#!/bin/bash
openapi-generator generate -g typescript-angular -i http://localhost:8080/api/v1/ip/v3/api-docs.yaml -o ip2location-client --additional-properties npmName=@ip2location/api,snapshot=true,ngVersion=9.0.0
ng-swagger-gen -i http://localhost:8080/api/v1/ip/v3/api-docs.yaml -o ip2location-client #swagger only
ng-openapi-gen --input http://localhost:8080/api/v1/ip/v3/api-docs.yaml --output src/app/api
