import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {Ip2LocationRecordCacheModel} from '../model/ip-2-location-record-cache.model';
import {catchError} from 'rxjs/operators';

const API_URL = `http://localhost:8080/api`;

@Injectable()
export class IpViewService {
  private ip2LocationApiUrl = `${API_URL}/v1/ip/all`;

  private dataStore: {
    ip2locationArr: Ip2LocationRecordCacheModel[]
  };

  private subject: BehaviorSubject<Ip2LocationRecordCacheModel[]>;

  constructor(private http: HttpClient) {
    this.dataStore = {ip2locationArr: []};
    this.subject = new BehaviorSubject<Ip2LocationRecordCacheModel[]>([]);
  }

  getIps(): Observable<Ip2LocationRecordCacheModel[]> {
    console.log('calling API');
    console.log(this.ip2LocationApiUrl);
    return this.http.get<Ip2LocationRecordCacheModel[]>(`${this.ip2LocationApiUrl}`);
  }


  // getJSON() {
  //   return this.http.get<Ip2LocationRecordCacheModel[]>('assets/mock_all_ips.json')
  //     .subscribe(data => {
  //       this.dataStore.ip2locationArr = data;
  //       this.subject.next(Object.assign({}, this.dataStore).ip2locationArr);
  //     }), catchError(error => {
  //     return throwError('Unable to fetch set');
  //   });
  //
  // }

  getJSON(): Observable<Ip2LocationRecordCacheModel[]> {
    // return this.http.get<Ip2LocationRecordCacheModel[]>('assets/mock_all_ips.json')
    //   .subscribe(data => {
    //     this.dataStore.ip2locationArr = data;
    //     this.subject.next(Object.assign({}, this.dataStore).ip2locationArr);
    //   }), catchError(error => {
    //   return throwError('Unable to fetch set');
    // });
    return this.http.get<Ip2LocationRecordCacheModel[]>('assets/mock_all_ips.json');
  }
}
