import {Injectable, isDevMode} from '@angular/core';


@Injectable()
export class LinksService {
  private _DISPATCH_ADDRESS: string;
  private _BILLING_ADDRESS: string;
  private _AUTHORIZATION_ADDRESS: string;

  /* Main page */
  private _HOME_PAGE_URL = '/home';

  /*Sms dispatch*/
  private _GET_DISPATCHES_URL = '/sms-dispatches';
  private _ADD_DISPATCH_URL = '/sms-dispatches';
  private _DELETE_DISPATCH_URL = '/sms-dispatches';
  private _ADD_SMS_TO_DISPATCH_URL = '/sms-dispatches/';
  private _EDIT_SMS_IN_DISPATCH_URL = '/sms-dispatches/';
  private _DELETE_MESSAGE_FROM_DISPATCH_URL = '/sms-dispatches/';
  private _GET_DISPATCH_BY_ID_URL = '/sms-dispatches/';
  private _RESEND_SMS = '/resend-sms/';
  private _GET_SMS_COST = '/sms/cost';


  /*contact group*/
  private _GET_ALL_GROUPS_URL = '/groups';
  private _GET_MANUALLY_CREATED_GROUPS_URL = '/manually-created-groups';
  private _LOAD_CONTACTS_FROM_FILE_URL = '/groups/load-contacts';
  private _CREATE_GROUP_URL = '/groups';
  private _DELETE_GROUP_URL = '/groups/';
  private _EDIT_GROUP_URL = '/groups/';
  private _DUPLICATE_AUTO_DELETE = '/groups/delete-duplicates/auto';
  private _DUPLICATE_MANUAL_DELETE = '/groups/delete-duplicates/manual';


  /*billing*/
  private _GET_BALANCE_URL = '/balance';
  private _GET_PAYMENTS_URL = '/payments';
  private _GET_SMS_TARIF = '/sms/tarif';
  private _ADD_PAYMENT_URL = '/payment/add';
  private _INTERKASSA_ID = '58d25a763b1eaf88258b4567';

  /*authentication*/
  private _GET_OAUTH_TOKEN = '/oauth/token';
  private _GET_REGISTRATION_URL = '/maintainance/register-user';

  /*profile*/
  private _GET_CURRENT_USER = '/profile';
  private _CHANGE_PASSWORD = '/profile/change-password';
  private _EDIT_PROFILE = '/profile/edit-profile';


  constructor() {
    if (isDevMode()) {
      this._DISPATCH_ADDRESS = 'http://localhost:8002';
      this._BILLING_ADDRESS = 'http://localhost:8003';
      this._AUTHORIZATION_ADDRESS = 'http://localhost:9191/uaa';
    } else {
      this._DISPATCH_ADDRESS = 'http://localhost:9999/dispatch-service';
      this._BILLING_ADDRESS = 'http://localhost:9999/billing-service';
      this._AUTHORIZATION_ADDRESS = 'http://localhost:9191/uaa';
    }
  }

  get GET_HOME_URL(): string {
    return this._HOME_PAGE_URL;
  }

  get GET_OAUTH_TOKEN(): string {
    return this._AUTHORIZATION_ADDRESS + this._GET_OAUTH_TOKEN;
  }

  get GET_REGISTRATION_URL(): string {
    return this._AUTHORIZATION_ADDRESS + this._GET_REGISTRATION_URL;
  }

  get GET_SMS_COST(): string {
    return this._BILLING_ADDRESS + this._GET_SMS_COST;
  }

  get RESEND_SMS(): string {
    return this._DISPATCH_ADDRESS + this._RESEND_SMS;
  }

  get GET_ALL_GROUPS_URL(): string {
    return this._DISPATCH_ADDRESS + this._GET_ALL_GROUPS_URL;
  }

  get GET_MANUALLY_CREATED_GROUPS_URL(): string {
    return this._DISPATCH_ADDRESS + this._GET_MANUALLY_CREATED_GROUPS_URL;
  }

  get LOAD_CONTACTS_FROM_FILE_URL(): string {
    return this._DISPATCH_ADDRESS + this._LOAD_CONTACTS_FROM_FILE_URL;
  }

  get CREATE_GROUP_URL(): string {
    return this._DISPATCH_ADDRESS + this._CREATE_GROUP_URL;
  }

  get DELETE_GROUP_URL(): string {
    return this._DISPATCH_ADDRESS + this._DELETE_GROUP_URL;
  }

  get EDIT_GROUP_URL(): string {
    return this._DISPATCH_ADDRESS + this._EDIT_GROUP_URL;
  }

  get DUPLICATE_AUTO_DELETE(): string {
    return this._DISPATCH_ADDRESS + this._DUPLICATE_AUTO_DELETE;
  }

  get DUPLICATE_MANUAL_DELETE(): string {
    return this._DISPATCH_ADDRESS + this._DUPLICATE_MANUAL_DELETE;
  }

  get GET_DISPATCH_BY_ID_URL(): string {
    return this._DISPATCH_ADDRESS + this._GET_DISPATCH_BY_ID_URL;
  }

  get GET_DISPATCHES_URL(): string {
    return this._DISPATCH_ADDRESS + this._GET_DISPATCHES_URL;
  }

  get ADD_DISPATCH_URL(): string {
    return this._DISPATCH_ADDRESS + this._ADD_DISPATCH_URL;
  }

  get DELETE_DISPATCH_URL(): string {
    return this._DISPATCH_ADDRESS + this._DELETE_DISPATCH_URL;
  }

  get ADD_SMS_TO_DISPATCH_URL(): string {
    return this._DISPATCH_ADDRESS + this._ADD_SMS_TO_DISPATCH_URL;
  }

  get EDIT_SMS_IN_DISPATCH_URL(): string {
    return this._DISPATCH_ADDRESS + this._EDIT_SMS_IN_DISPATCH_URL;
  }

  get DELETE_MESSAGE_FROM_DISPATCH_URL(): string {
    return this._DISPATCH_ADDRESS + this._DELETE_MESSAGE_FROM_DISPATCH_URL;
  }

  get GET_BALANCE_URL(): string {
    return this._BILLING_ADDRESS + this._GET_BALANCE_URL;
  }

  get ADD_PAYMENT_URL(): string {
    return this._BILLING_ADDRESS + this._ADD_PAYMENT_URL;
  }

  get GET_PAYMENTS_URL(): string {
    return this._BILLING_ADDRESS + this._GET_PAYMENTS_URL;
  }

  get GET_CURRENT_USER(): string {
    return this._AUTHORIZATION_ADDRESS + this._GET_CURRENT_USER;
  }

  get CHANGE_PASSWORD(): string {
    return this._AUTHORIZATION_ADDRESS + this._CHANGE_PASSWORD;
  }

  get EDIT_PROFILE(): string {
    return this._AUTHORIZATION_ADDRESS + this._EDIT_PROFILE;
  }

  get GET_SMS_TARIF_URL(): string {
    return this._BILLING_ADDRESS + this._GET_SMS_TARIF;
  }

  get GET_INTERKASSA_URL(): string {
    return this._INTERKASSA_ID;
  }
}
