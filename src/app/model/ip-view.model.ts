import {Ip2LocationRecordCacheModel} from './ip-2-location-record-cache.model';

export interface IpViewModel {
  code?: number;
  data?: Ip2LocationRecordCacheModel;
  message?: string;
  requestId?: string;
}
