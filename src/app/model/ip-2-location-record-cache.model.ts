export interface Ip2LocationRecordCacheModel {
  cache_rec_id?: number;
  city_name?: string;
  country_code?: string;
  country_name?: string;
  created?: string;
  desc_text?: string;
  domain?: string;
  ip?: string;
  isp?: string;
  latitude?: number;
  longitude?: number;
  region_name?: string;
  time_zone?: string;
  updated?: string;
  updated_times?: number;
  zip_code?: string;
}
