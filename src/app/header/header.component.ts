import { IpViewService } from './../service/ip-view.service';
import { Component } from '@angular/core';
import { Balance } from '../domain/Balance';
import { Ip2LocationRecordCacheModel } from '../model/ip-2-location-record-cache.model';

@Component({
  selector: 'app-header',
  templateUrl: `./header.component.html`,
})

export class HeaderComponent {
  balance: Balance = new Balance();
  ips: Ip2LocationRecordCacheModel[];

  constructor(private ipViewService: IpViewService) {
    //TODO: map each IP to balance
    ipViewService.getIps().subscribe(
      value => {
        this.balance.ip = value[0];
        this.balance.currency = 'грн';
      },
      error => console.log(error)
    );
  }
}
