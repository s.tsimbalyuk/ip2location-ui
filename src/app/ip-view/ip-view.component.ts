import {Component, OnInit} from '@angular/core';
import {IpViewService} from '../service/ip-view.service';
import {Ip2LocationRecordCacheModel} from '../model/ip-2-location-record-cache.model';

@Component({
  selector: 'app-ip-view',
  templateUrl: './ip-view.component.html',
  styleUrls: ['./ip-view.component.scss'],
  providers: [IpViewService]
})
export class IpViewComponent implements OnInit {

  ips: Ip2LocationRecordCacheModel[];
  cols: any[];

  constructor(private ipViewService: IpViewService) {
  }

  ngOnInit() {
    this.getIps();

    this.cols = [
      {field: 'ip', header: 'IP'},
      {field: 'isp', header: 'ISP'},
      {field: 'city_name', header: 'City Name'},
      {field: 'country_name', header: 'Country Name'},
      {field: 'latitude', header: 'Latitude'},
      {field: 'longitude', header: 'Longitude'}
    ];
  }

  getIps() {
    this.ipViewService.getJSON().subscribe(
      response => {
        // this.ips = response.filter(notEmpty);
        this.ips = response;
        console.log(this.ips);
        console.log(response);
      },
      error => {
        console.error(error);
      }
    );
  }

  updateIps() {
    this.ipViewService.getIps().subscribe(
      value => {
        this.ips = value;
        console.log(this.ips);
        console.log(value);
      },
      error => {
        console.error(error);
      }
    );
  }
}

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
  return value !== null && value !== undefined;
}

