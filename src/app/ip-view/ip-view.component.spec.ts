import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IpViewComponent } from './ip-view.component';

describe('StoredIpsComponent', () => {
  let component: IpViewComponent;
  let fixture: ComponentFixture<IpViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IpViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IpViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
