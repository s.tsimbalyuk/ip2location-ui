import { Ip2LocationRecordCacheModel } from '../model/ip-2-location-record-cache.model';

export class Balance {
  amount: number = 0;
  currency: string = 'грн';
  ip: Ip2LocationRecordCacheModel;
}
